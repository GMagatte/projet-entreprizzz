# **Projet Entreprizzz**

### _Gestion budget de l'entreprise_

[GitLab](https://gitlab.com/GMagatte/projet-entreprizzz) &  [Simplonline](https://simplonline.co/briefs/fdefb743-f07e-43b2-ba1c-28c0905c1317)

##### Choix de conception

Ce projet est la création d'une application qui permettra aux employés d'une grande entreprise de commander du matériel. Pour ce faire, nous avons défini verbalement les acteurs qui utiliseront cette application, ainsi que le déroulement des actions qui interviendront lors d'une commande. L'utilisateur/employé de l'entreprise sur cette application choisira du matériel selon un budget qui lui sera alloué, ensuite passera sa commande. Cette commande arrivera alors à son chef de service qui, lui, décidera si celle-ci est viable ou non, et de ce fait validera ou non cette demande d'achat. Cette commande ira ensuite au service achats de l'entreprise et ce service fera alors les achats demandés. Sur cette demande d'achats, il faut notamment que les chefs de service puissent consulter l'historique des dépenses de leur service, tout comme le service achats, le service comptabilité et la direction générale.

Afin de monter le projet sereinement, nous avons créé différents diagrammes, ce qui nous permettra d'avoir une meilleure visibilité sur celui-ci. Pour ce faire, nous avons créé :
  - un diagramme de cas d'utilisations détaillé (usecase)
  - un diagramme de modèle
  - un diagramme de classes détaillé

---
###### Diagramme de usecase :

- Le premier acteur du usecase est un **salarié** de l'entreprise.
    - Il peut ensuite regarder le catalogue pour qu'il puisse passer une commande de matériel ou de logiciel. 
    - Il peut subséquemment selon ses envies et son budget, choisir ses articles.
    - Après sélection de tous ses articles, il peut passer commande.
- Le second acteur est le **chef de service** du salarié.
    - Celui-ci reçoit la demande de commande. Commande qu'il peut donc parcourir afin de la valider ou pas.
    - Cette personne a aussi la main sur l'historique de tous les achats et commandes de tous les salariés de son service. 
- Le troisième acteur correspond à un **employé du service achat**.
    - Celui-ci prend donc connaissance de la commande validée du salarié et peut ainsi procéder au paiement de cette dernière.
- Les deux derniers acteurs sont le **service comptabilité** et la **direction générale**.
    - Ils peuvent afficher l'historique des recettes et dépenses de l'entreprise, et par la même le service demandeur, et la catégorie de la dépense réalisée.

---
###### Diagramme de modèle :

En nous aidant du diagramme de usecase, nous avons pu ainsi déterminer les différentes classes nécessaires à la bonne construction de notre projet.
- En premier, nous avons une classe **Utlisateur** qui représentera selon la situation l'employé (salarié), un chef de service, ou tout autre personne de l'entreprise. 
- Ensuite, nous avons la classe **Commande**. Celle-ci est un point central de l'application, permettant après sélection des articles désirés de passer commande pour que le service achats puisse procéder. Cette classe commande est relié à la classe Utilisateur, à la classe Matériel ainsi qu'à la classe LigneDeComande. 
- On a fait la table **LigneDeComande** afin de faciliter la prise de commande de matériel. En effet, c'est une table de connexion qui est entre les classes Commande et Matériel et qui permet de passer une commande plus aisément.
- Reliée cette classe Commande donc, il y a la classe **Facture**, qui permet d'avoir le récapitulatif de la commande passée.
- Pour continuer, il y a la classe **Materiel**, reliée elle aussi à Commande. Cette classe définit tous les attributs qu'un matériel peut avoir. Et pour ce faire, cette classe est aussi reliée aux deux autres classes suivantes.
- La classe **Stock**, définit quant à elle la liste des matériaux disponible en réserve.
- La classe **Category** sert à créer différentes catégories utilisées lors de la création des articles. Celles-ci servent à lier les articles entre eux afin de constituer des pages de catégories listant tous les articles similaires. 

---
###### Diagramme de classe :

Afin de créer ce diagramme, nous reprenons le diagramme de modèle que nous améliorons en le complétant. En effet, nous avons par ce procédé toutes nos classes, auxquelles nous rajoutons les interfaces et répository, en détaillant par la même les méthodes qui seront utilisées, tout en sachant que chaque interfaces et répository contient le CRUD de base (Create, Read, Update et Delete).
- À la classe Utilisateur, nous avons donc l'interface **Iutilisateur** (I majuscule pour représenter "interface"). Cette interface rajoute les méthodes *getByPoste()* et *getByService()* qui permet de trier les utilisateurs selon soit leur poste, soit leur service.
- Avec la classe Facture, il y a l'interface **Ifacture** qui contient donne notamment les méthodes *getByModePayement()*, *getByNumeroCommande()* et *getByDate()*. Ces méthodes permettent de trier les factures, dans l'ordre, soit par le mode de paiement, soit par le numéro de commande, soit par date.
- La classe Commande a comme interface **Icommande**, qui elle rajoute en plus du crud les méthodes *getByUser()* qui permet de trier par les commandes par utilisateurs, *getByDate()* qui permet de trier par date, *getByDateValidation()* qui trie par date de validation, *order()* qui permet de passer la commande pour que celle-ci aille au service achat, et *validation()* qui permet au chef de service de valider ou on la commande.
- Enfin, il y a l'interface **Imateriel** reliée à la classe Materiel, et qui rajoute les méthodes *getByName()* et *getByCategory()* qui permet de trier par nom de matériel et par catégorie de matériel.